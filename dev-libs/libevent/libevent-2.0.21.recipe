SUMMARY="Libevent - an event notification library"
DESCRIPTION="The libevent API provides a mechanism to execute a callback function when a specific event occurs on a file descriptor or after a timeout has been reached. Furthermore, libevent also support callbacks due to signals or regular timeouts.
libevent is meant to replace the event loop found in event driven network servers. An application just needs to call event_dispatch() and then add or remove events dynamically without having to change the event loop."
HOMEPAGE="http://www.libevent.org/"
SRC_URI="https://github.com/downloads/libevent/libevent/libevent-2.0.21-stable.tar.gz"
CHECKSUM_MD5="b2405cc9ebf264aa47ff615d9de527a2"
LICENSE="BSD (3-clause)"
COPYRIGHT="2000-2007 Niels Provos, 2005 Nick Mathewson, and other contributors."

REVISION="1"

ARCHITECTURES="x86_gcc2 x86"

SECONDARY_ARCHITECTURES="x86_gcc2 x86"

PROVIDES="
	cmd:event_rpcgen.py
	lib:libevent$secondaryArchSuffix = 2.0_5.1.9 compat >= 2.0_5
	lib:libevent$secondaryArchSuffix = 5.1.9 compat >= 5
	lib:libevent_2.0$secondaryArchSuffix = 5.1.9 compat >= 5
	lib:libevent_core_2.0$secondaryArchSuffix = 5.1.9 compat >= 5
	lib:libevent_extra_2.0$secondaryArchSuffix = 5.1.9 compat >= 5
	lib:libevent_pthreads_2.0$secondaryArchSuffix = 5.1.9 compat >= 5
	lib:libevent_openssl_2.0$secondaryArchSuffix= 5.1.9 compat >= 5
	lib:libevent_core$secondaryArchSuffix = 5.1.9 compat >= 5
	lib:libevent_extra$secondaryArchSuffix = 5.1.9 compat >= 5
	lib:libevent_pthreads$secondaryArchSuffix = 5.1.9 compat >= 5
	lib:libevent_openssl$secondaryArchSuffix= 5.1.9 compat >= 5
	"
REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libnetwork$secondaryArchSuffix >= $haikuVersion
	lib:libssl$secondaryArchSuffix
	lib:libcrypto$secondaryArchSuffix
	lib:libnetwork >= $haikuVersion
	"
BUILD_REQUIRES="
	devel:libssl$secondaryArchSuffix
	devel:libcrypto$secondaryArchSuffix
	devel:libz
	"

BUILD_PREREQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	cmd:aclocal
	cmd:autoconf
	cmd:automake
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:libtoolize
	cmd:make
	"
SOURCE_DIR="$portVersionedName-stable"

PATCHES="libevent-2.0.21.patch"

BUILD()
{
	sed -i 's/AM_CONFIG_HEADER/AC_CONFIG_HEADERS/g' configure.in
	sed -i 's/TESTS = $(top_srcdir)/TESTS = {$sourceDir}/g' test/Makefile.am

	libtoolize --force --copy --install
	aclocal
	autoconf
	automake --add-missing
	runConfigure ./configure
	make $jobArgs
}

INSTALL()
{
	make install
	prepareInstalledDevelLibs \
		libevent \
		libevent_core \
		libevent_extra \
		libevent_pthreads \
		libevent_openssl

	mv .libs/libevent_core.* \
		.libs/libevent_extra.* \
		.libs/libevent_pthreads.* \
		.libs/libevent_openssl.* \
		$developLibDir
	fixPkgconfig

	# devel package
	packageEntries devel \
		$developDir
}

# ----- devel package -------------------------------------------------------

PROVIDES_devel="
	cmd:event_rpcgen.py
	lib:libevent$secondaryArchSuffix = $portVersion
	lib:libevent_2.0$secondaryArchSuffix = 5.1.9 compat >= 5
	lib:libevent_core_2.0$secondaryArchSuffix = 5.1.9 compat >= 5
	lib:libevent_extra_2.0$secondaryArchSuffix = 5.1.9 compat >= 5
	lib:libevent_pthreads_2.0$secondaryArchSuffix = 5.1.9 compat >= 5
	lib:libevent_openssl_2.0$secondaryArchSuffix = 5.1.9 compat >= 5
	devel:libevent$secondaryArchSuffix = 2.0_5.1.9 compat >= 2.0_5
	devel:libevent$secondaryArchSuffix = 5.1.9 compat >= 5
	devel:libevent_core_2.0$secondaryArchSuffix = 5.1.9 compat >= 5
	devel:libevent_extra_2.0$secondaryArchSuffix = 5.1.9 compat >= 5
	devel:libevent_pthreads_2.0$secondaryArchSuffix = 5.1.9 compat >= 5	
	devel:libevent_openssl_2.0$secondaryArchSuffix = 5.1.9 compat >= 5
	devel:libevent_core$secondaryArchSuffix = 5.1.9 compat >= 5
	devel:libevent_extra$secondaryArchSuffix = 5.1.9 compat >= 5
	devel:libevent_pthreads$secondaryArchSuffix = 5.1.9 compat >= 5	
	devel:libevent_openssl$secondaryArchSuffix = 5.1.9 compat >= 5
	"

REQUIRES_devel="
	libevent$secondaryArchSuffix  == $portVersion base
	devel:libssl
	devel:libcrypto
	"
